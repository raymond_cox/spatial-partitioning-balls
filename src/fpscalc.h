#ifndef FPSCALC_H
#define FPSCALC_H
#include "glh.h"
class fpscalc {
public:
   fpscalc();
   int getfps();
   void tick();
private:
   int fps;
   int frames;
   int lastupdate;
};
#endif
