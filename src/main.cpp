/*
   Sequential balls
*/

#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include <math.h>
#include <time.h>
#include <vector>
#include "glh.h"
#include "color.h"
#include "ball.h"
#include "vector2d.h"
#include "constants.h"
#include "fpscalc.h"
using namespace std;
// Function pronotypes
void InitGL();
void InitBalls(int radius, int numcol, int numrow);
void UpdateTitle();
void UpdateVelocities(int deltatime);
void UpdateSideCollision(ball &ball);
void UpdateCollisions();
void UpdatePositions();
void FixGridPosition(vector2d &pos);
bool ValidGridPosition(vector2d &pos);
void MouseBalls(int x, int y);
vector2d GetGridPosition(vector2d pos);
vector2d* GetAdjGridPositions(vector2d gridpos);
GLvoid DrawGLScene();
GLvoid Idle();
GLvoid Reshape(int w, int h);
GLvoid MouseClick(int button, int state, int x, int y);
GLvoid MouseMove(int x, int y);
GLvoid ProcessKeys(unsigned char key, int x, int y);
// Global variables
ball balls[TOTAL_BALLS];
vector<int> grid[GRID_WIDTH][GRID_HEIGHT];
fpscalc fps;
bool solid = SOLID;
int mbutton = 0;

int main(int argc, char *argv[]) {
   srand ( time(NULL) );
   glutInit(&argc, argv);
   glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGB);
   glutInitWindowSize (WIDTH, HEIGHT);
   int centerX = (glutGet(GLUT_SCREEN_WIDTH)-WIDTH)/2;
   int centerY = (glutGet(GLUT_SCREEN_HEIGHT)-HEIGHT)/2;
   glutInitWindowPosition (centerX, centerY);
   glutCreateWindow ("Balls");
   InitGL();
   InitBalls(BALLS_RADIUS,BALLS_WIDTH,BALLS_HEIGHT);
   glutKeyboardFunc(ProcessKeys);
   glutDisplayFunc(DrawGLScene);
   glutReshapeFunc(Reshape);
   glutIdleFunc(Idle);
   glutMouseFunc(MouseClick);
   glutMotionFunc(MouseMove);
   printf("Grid: (%i,%i)\n",GRID_WIDTH,GRID_HEIGHT);
   glutMainLoop();
   return 0;
}

void InitBalls(int radius, int numcol, int numrow) {
   int diameter = radius * 2;
   int cx = WIDTH/2; 
   int ccx = cx-radius*numcol;
   int cy = HEIGHT/2;
   int i=0;
   for (float x=radius+ccx; x<=(diameter+0.1)*numcol+ccx; x += (diameter+0.1)) {
      for (float y=radius; y<=(diameter+0.1)*numrow; y += (diameter+0.1)) {
         float r=rand()%11/10.0;float g=rand()%11/10.0;float b=rand()%11/10.0;
         color col(r,g,b);
         vector2d pos(x+i/(numcol*numrow+0.0),y+i/(numcol*numrow+0.0));
         ball newball(radius,col,pos);
         newball.solid = solid;
         balls[i] = newball;
         vector2d gridpos = GetGridPosition(newball.position);
         int gx = gridpos.x;
         int gy = gridpos.y;
         grid[gx][gy].push_back(i);
         i++;
      }
   }
}
void InitGL() {
   glClearColor(0.0, 0.0, 0.0, 0.0);
   glMatrixMode (GL_PROJECTION);
   glLoadIdentity();
   glOrtho (0, WIDTH, HEIGHT, 0, 0, 1);
   glMatrixMode (GL_MODELVIEW);
   glDisable(GL_DEPTH_TEST);
}
GLvoid DrawGLScene() {
   static int prevtime=glutGet(GLUT_ELAPSED_TIME);
   int deltatime = glutGet(GLUT_ELAPSED_TIME) - prevtime;
   prevtime = glutGet(GLUT_ELAPSED_TIME);
   glLoadIdentity();
   glClear(GL_COLOR_BUFFER_BIT);
   // Update balls velocities
   UpdateVelocities(deltatime);
   // Check for collisions on the updated balls
   for (int i=0; i<CHECKS; i++) UpdateCollisions();
   // Move balls
   UpdatePositions();
   glutSwapBuffers();
}
GLvoid Idle() {
   glutPostRedisplay();
   fps.tick();
   UpdateTitle();
	#ifdef _WIN32
	   Sleep(10);
	#else
	   usleep(1000);
	#endif
}
GLvoid Reshape(int w, int h) {
   //glViewport (0, 0, (GLsizei)(w), (GLsizei)(h));
}
GLvoid MouseClick(int button, int state, int x, int y) {
   mbutton = button;
   if (state == 0) {
      if (button == 2) {
         solid = !solid;
         MouseBalls(x,y);
      } else if (button == 0) {
         MouseBalls(x,y);
      }
   }
}
GLvoid MouseMove(int x, int y) {
   MouseBalls(x,y);
}
void MouseBalls(int x, int y) {
   y -= (glutGet(GLUT_WINDOW_HEIGHT)-HEIGHT);
   vector2d gridpos = GetGridPosition(vector2d(x,y));
   vector2d *adjpoints = GetAdjGridPositions(gridpos);
   for (int a=0; a<9; a++) {
      if (ValidGridPosition(adjpoints[a])) {
         int gx = adjpoints[a].x;
         int gy = adjpoints[a].y;
         for (int b=0; b<grid[gx][gy].size(); b++) {
            int ballindex = grid[gx][gy][b];
            ball &curball = balls[ballindex];
            if (mbutton == 0) {
               curball.velocity.x += rand()%30-15;
               curball.velocity.y += rand()%30-15;
            } else {
               curball.solid = solid;
            }
         }
      }
   }
   free(adjpoints);
}
GLvoid ProcessKeys(unsigned char key, int x, int y) {
}
void UpdateTitle() {
   char buf[40];
   sprintf( buf, "FPS: %d Balls: %d",fps.getfps(),BALLS_WIDTH*BALLS_HEIGHT);
   glutSetWindowTitle(buf); 
}
void UpdateVelocities(int deltatime) {
   // Update balls velocities
   for (int i=0; i<TOTAL_BALLS; i++) {
      ball &curball = balls[i];
      curball.velocity.y += GRAVITY * deltatime;
   }
}
void UpdateCollisions() { 
   for (int i=0; i<TOTAL_BALLS; i++) {
      ball &curball = balls[i];
      UpdateSideCollision(curball);
      vector2d *adjpoints = GetAdjGridPositions(GetGridPosition(curball.position));
      for (int a=0; a<9; a++) {
         if (ValidGridPosition(adjpoints[a])) {
            int gx = adjpoints[a].x;
            int gy = adjpoints[a].y;
            for (int b=0; b<grid[gx][gy].size(); b++) {
               int ballindex = grid[gx][gy][b];
               ball &otherball = balls[ballindex];
               if (curball.iscolliding(otherball)) {
                  curball.resolvecollision(otherball);
               }
            }
         }
      }
      free(adjpoints);
   }
}
void UpdatePositions() {   
   // Clear the grid..
   for (int x=0; x<GRID_WIDTH; x++) {
      for (int y=0; y<GRID_HEIGHT; y++) {
         grid[x][y].clear();
      }
   }
   for (int i=0; i<TOTAL_BALLS; i++) {
      ball &curball = balls[i];
      curball.position += curball.velocity;
      glPushMatrix();
      glTranslatef(curball.position.x, curball.position.y,0.0);
      curball.render();
      glPopMatrix();
      vector2d gpos = GetGridPosition(curball.position);
      int gx, gy;
      gx = gpos.x;
      gy = gpos.y;
      grid[gx][gy].push_back(i);
   }
}
vector2d GetGridPosition(vector2d v) {
   int x = floor((v.x-BALLS_RADIUS)/(BALLS_RADIUS*2)+0.5);
   int y = floor((v.y-BALLS_RADIUS)/(BALLS_RADIUS*2)+0.5);
   vector2d gridpos(x,y);
   FixGridPosition(gridpos);
   return gridpos;
}
void FixGridPosition(vector2d &pos) {
   if (pos.x >= GRID_WIDTH) pos.x = GRID_WIDTH-1;
   if (pos.x < 0) pos.x = 0;
   if (pos.y >= GRID_HEIGHT) pos.y = GRID_HEIGHT-1;
   if (pos.y < 0) pos.y = 0;
}
bool ValidGridPosition(vector2d &pos) {
   if (pos.x >= GRID_WIDTH) return false;
   if (pos.x < 0) return false;
   if (pos.y >= GRID_HEIGHT) return false;
   if (pos.y < 0) return false;
   return true;
}
void UpdateSideCollision(ball &curball) {
   // bottom screen
   if (curball.position.y > HEIGHT-curball.radius) {
      curball.position.y = HEIGHT-curball.radius;
      curball.velocity.y *= -SIDE_BOUNCE;
   }
   // top screen
   if (curball.position.y < curball.radius) {
      curball.position.y = curball.radius*2;
      curball.velocity.y *= -SIDE_BOUNCE;
   }
   // left screen
   if (curball.position.x < curball.radius) {
      curball.position.x = curball.radius;
      curball.velocity.x *= -SIDE_BOUNCE;
   }
   // right screen
   if (curball.position.x > WIDTH-curball.radius) {
      curball.position.x = WIDTH-curball.radius;
      curball.velocity.x *= -SIDE_BOUNCE;
   }
}
vector2d* GetAdjGridPositions(vector2d gpos) {
   vector2d* adjpos = (vector2d*)malloc(sizeof(vector2d)*9);
   vector2d rpos(gpos.x+1, gpos.y);
   vector2d lpos(gpos.x-1, gpos.y);
   vector2d dpos(gpos.x, gpos.y-1);
   vector2d upos(gpos.x, gpos.y+1);
   vector2d urpos(upos.x+1, upos.y);
   vector2d ulpos(upos.x-1, upos.y);
   vector2d drpos(dpos.x+1, dpos.y);
   vector2d dlpos(dpos.x-1, dpos.y);
   adjpos[0] = rpos;
   adjpos[1] = lpos;
   adjpos[2] = dpos;
   adjpos[3] = upos;
   adjpos[4] = urpos;
   adjpos[5] = ulpos;
   adjpos[6] = drpos;
   adjpos[7] = dlpos;
   adjpos[8] = gpos;
   return adjpos;
}
