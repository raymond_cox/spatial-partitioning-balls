#ifndef COLOR_H
#define COLOR_H
#include "glh.h"

class color {
public:
   color(GLfloat red, GLfloat green, GLfloat blue);
   color();   
   GLfloat r;
   GLfloat g;
   GLfloat b;
};
#endif
