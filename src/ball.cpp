#include "ball.h"
#include "constants.h"

ball::ball() {
}

ball::ball(float cradius, color ccolor, vector2d cposition) {
   radius = cradius;
   col = ccolor;
   position = cposition;
   solid = true;
}

void ball::render() {
   glColor3f(col.r,col.g,col.b);
   if (solid) glBegin(GL_TRIANGLE_FAN);
   else glBegin(GL_LINE_LOOP);
   for (float i=0.0; i<360.0; i+= QUALITY) {
      glVertex2f(sin(i) * radius, cos(i) * radius);
   }
   glEnd();
}

void ball::resolvecollision(ball &otherball) {
   // Move balls so they are not intersecting
   vector2d delta = position - otherball.position;
   float d = delta.getlength();
   if (d > 0) {
      vector2d mtd = delta * (((radius + otherball.radius)-d)/d);
      position += mtd/2;
      otherball.position -= mtd/2;

      // Impact speed
      vector2d v = velocity-otherball.velocity; 
      float vn = v.dot(mtd.normalize());
      // If balls are moving towards each other
      if (vn <= 0.0) {
         // Update balls velocities
         float i = (-(1 + ELASTICITY) * vn);
         vector2d impulse = mtd * i;
         velocity += impulse;
         otherball.velocity -= impulse;

      }
   }
}

bool ball::iscolliding(ball otherball) {
   vector2d otherpos = otherball.position;
   if (position.distance(otherpos) <= otherball.radius + radius) {
      return true;
   } else {
      return false;
   }
}
