#ifndef BALL_H
#define BALL_H

#include "glh.h"
#include "vector2d.h"
#include "color.h"
#include <cmath>

class ball {
public:
   ball(float cradius, color ccolor, vector2d cposition);
   ball();
   bool iscolliding(ball ball1);
   void resolvecollision(ball &otherball);
   void render();
   color col;
   float radius;
   bool solid;
   vector2d position;
   vector2d velocity;
};

#endif
