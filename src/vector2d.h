#ifndef VECTOR2D_H
#define VECTOR2D_H

class vector2d {
public:
   vector2d(float cx, float cy);
   vector2d();
   vector2d& operator+=(const vector2d &v);
   const vector2d operator+(const vector2d &v) const;
   vector2d& operator-=(const vector2d &v);
   const vector2d operator-(const vector2d &v) const;
   vector2d& operator*=(const vector2d &v);
   const vector2d operator*(const vector2d &v) const;
   vector2d& operator*=(const float &i);
   const vector2d operator*(const float &i) const;
   vector2d& operator/=(const float &i);
   const vector2d operator/(const float &i) const;

   vector2d& normalize();
   float dot(vector2d v);
   float distance(vector2d othervector);
   float getlength();
   float x;
   float y;
};
#endif
