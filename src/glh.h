#ifndef GLH_H
#define GLH_H

#ifdef __APPLE__
   #include <OpenGL/gl.h>
   #include <OpenGL/glu.h>
   #include <GLUT/glut.h>
   #include <unistd.h>
#elif _WIN32
	#include <windows.h>
	#include <gl\gl.h>
	#include <gl\glu.h>
	#include <gl\glut.h>
// Linux
#else
   #include <GL/gl.h>
   #include <GL/glu.h>
   #include <GL/glut.h>
   #include <unistd.h>
#endif

#endif
