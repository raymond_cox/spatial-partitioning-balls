#include "vector2d.h"
#include <cmath>

vector2d::vector2d(float cx, float cy) {
   x = cx;
   y = cy;
}

vector2d::vector2d() {
   x=0;
   y=0;
}
vector2d& vector2d::operator+=(const vector2d &v) {
   x += v.x;
   y += v.y;
   return *this;
}

const vector2d vector2d::operator+(const vector2d &v) const {
   return (vector2d(x + v.x, y + v.y)); 
}

vector2d& vector2d::operator-=(const vector2d &v) {
   x -= v.x;
   y -= v.y;
   return *this;
}

const vector2d vector2d::operator-(const vector2d &v) const {
   return (vector2d(x - v.x, y - v.y)); 
}

vector2d &vector2d::operator*=(const vector2d &v) {
   x *= v.x;
   y *= v.y;
   return *this;
}

const vector2d vector2d::operator*(const vector2d &v) const {
   return (vector2d(x * v.x, y * v.y)); 
}

vector2d &vector2d::operator*=(const float &i) {
   x *= i;
   y *= i;
   return *this;
}

const vector2d vector2d::operator*(const float &i) const {
   return (vector2d(x * i, y * i)); 
}

vector2d &vector2d::operator/=(const float &i) {
   float f = 1.0F/i;
   x *= f;
   y *= f;
   return *this;
}

const vector2d vector2d::operator/(const float &i) const {
   float f = 1.0F/i;
   return (vector2d(x * f, y * f)); 
}

vector2d& vector2d::normalize() {
   return (*this /= sqrtf(x * x + y * y));
}

float vector2d::dot(vector2d v) {
   return (v.x*x + v.y*y);
}

float vector2d::distance(vector2d othervector) {
   return sqrt((othervector.x-x)*(othervector.x-x) + (othervector.y-y)*(othervector.y-y));
}

float vector2d::getlength() {
   return sqrtf((x*x) + (y*y));
}
